# INT/KIF Tools

Python tools to work with INT (extension)/KIF (signature) files such as the ones used by Fruit of Grisaia.  
Unfortunately they are damn slow because of the way the files are encrypted, with the wrong endianness or something.  
I'll gladly update the tools with any idea that can reduce/remove the immense overhead of having to rotate every damn DWORD before and after decrypting the file.

There is also a script to decompress zlib'ed CatScene (CST) files.