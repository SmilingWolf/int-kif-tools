# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

#!/usr/bin/env python

import sys, os, binascii, struct
from array import array
from cStringIO import StringIO
from Crypto.Cipher import Blowfish

import mt
from insani import *

import time
start = time.time()

if len(sys.argv) <> 2:
    print 'Please give an archive filename and a desired output directory on the\ncommand line.'
    sys.exit(0)

def read_entry(infile):
    result = {}
    fileentry = infile.read(72)
    fileentry = array('B', fileentry)

    filename = array('B')
    for i in xrange(64):
        if fileentry[i] != 0x00:
            filename.append(fileentry[i])
    result["filename"] = filename.tostring()
    result["fileoffset"] = fileentry[64+3] << 24 | fileentry[64+2] << 16 | fileentry[64+1] << 8 | fileentry[64+0]
    result["filesize"] = fileentry[68+3] << 24 | fileentry[68+2] << 16 | fileentry[68+1] << 8 | fileentry[68+0]
    return result

arcfile = open(sys.argv[1], 'rb')
assert_string(arcfile, '\x4B\x49\x46\x00', ERROR_ABORT)
filenum = read_unsigned(arcfile)
print ('Extracting %d files' % filenum)

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    if indexarray[elem]['filename'] == '__key__.dat':
        mtseed = indexarray[elem]['filesize']
        print 'Seeding the Mersenne Twister PRNG. Seed: %08X' % mtseed
        mersenne = mt.MT19937(mtseed)
        blowfishkey = struct.pack("<I", mersenne.extract_number())
        print 'Inizializing Blowfish. Key: %s' % binascii.hexlify(blowfishkey).upper()
        break
    else:
        ciphertext = struct.pack('>I', (indexarray[elem]['fileoffset'] + elem) & 0xFFFFFFFF) + struct.pack('>I', indexarray[elem]['filesize'])
        msg = cipher.decrypt(ciphertext)
        indexarray[elem]['fileoffset'] = struct.unpack_from('>I', msg, 0)[0]
        indexarray[elem]['filesize'] = struct.unpack_from('>I', msg, 4)[0]

arcfile.close()
