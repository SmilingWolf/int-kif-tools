import string, sys, os, binascii, struct
from stat import *
from array import array
from cStringIO import StringIO
from Crypto.Cipher import Blowfish

import mt
from insani import *

# http://stackoverflow.com/a/234329
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

# Pass to it an obfuscated name -> get the deobfuscated name
# Pass to it a deobfuscated name -> get the obfuscated name
def deobf_name(name, seed):
    newname = list(name)
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    revalpha = alpha[::-1]
    randkey = mt.MT19937(seed).extract_number()
    randkey = ((randkey >> 24) + (randkey >> 16) + (randkey >> 8) + randkey) & 0xFF
    for nameletters in xrange(len(name)):
        for alphaletters in xrange(len(revalpha)):
            index = (randkey + alphaletters) % len(revalpha)
            if name[nameletters] == revalpha[index]:
                newname[nameletters] = alpha[alphaletters-nameletters]
                break
    name = ''.join(newname)
    return name

def calc_filenameseed(v_key):
    magic = 0x4C11DB7
    seed = 0xFFFFFFFF
    for char in xrange(len(v_key)):
        letter = ord(v_key[char])
        letter = letter << 24
        seed ^= letter
        for i in xrange(8):
            if seed > 0x7FFFFFFF:
                seed += seed
                seed &= 0xFFFFFFFF
                seed ^= magic
            else:
                seed += seed
                seed &= 0xFFFFFFFF
        seed = ~seed
        seed &= 0xFFFFFFFF
    return seed

def write_entry(outfile, entry):
    entry['filename'] += '\x00' * (64 - len(entry['filename']))
    outfile.write(entry['filename'])
    write_unsigned(outfile, entry['fileoffset'], INT_LENGTH, LITTLE_ENDIAN)
    write_unsigned(outfile, entry['filesize'], INT_LENGTH, LITTLE_ENDIAN)

if len(sys.argv) not in (3, 5):
    print 'Please give a input directory and a desired output archive filename'
    print 'Optional parameters: the PRNG seed to use to crypt the files while repacking'
    print '                     the password to deobfuscate the filenames'
    print '%s input_dir output_file [Filename_password] [PRNG_Seed]' % sys.argv[0]
    sys.exit(0)

dirname = sys.argv[1]
arcfile = open(sys.argv[2], 'wb')
if len(sys.argv) < 4:
    # From DenpaSoft's Unrated Version
    nameseed = calc_filenameseed('FW-UPL9EX5T')
else:
    nameseed = calc_filenameseed(sys.argv[3])

if len(sys.argv) < 5:
    # Found in config.int from DenpaSoft's Unrated Version
    # bf_seed = 'A23343DE'
    # Found in ptcl.int from DenpaSoft's Unrated Version
    bf_seed = 'DDB1E197'
else:
    bf_seed = sys.argv[4]
bf_seed = int(bf_seed, 16)

print 'Seeding the Mersenne Twister PRNG. Seed: %08X' % bf_seed
mersenne = mt.MT19937(bf_seed)
blowfishkey = struct.pack("<I", mersenne.extract_number())
print 'Inizializing Blowfish. Key: %s' % binascii.hexlify(blowfishkey).upper()
cipher = Blowfish.new(blowfishkey, Blowfish.MODE_ECB)

arcfile.write('\x4B\x49\x46\x00')

filesarray = []
for (dirpath, dirs, filenames) in walklevel(dirname, 1):
    for filename in filenames:
        fileentry = {}
        filepath = dirpath + '\\' + filename
        if S_ISREG(os.stat(filepath).st_mode):
            fileentry['filepath'] = filepath
            fileentry['filename'] = filename
            filesarray.append(fileentry)

write_unsigned(arcfile, len(filesarray) + 1)

entry = {}
entry['filename'] = '__key__.dat'
entry['fileoffset'] = 0x00
entry['filesize'] = bf_seed

write_entry(arcfile, entry)
write_zeroes(arcfile, (len(filesarray)) * 72)

write_entry(arcfile, entry)

indexbuffer = StringIO()
for file in xrange(len(filesarray)):
    infile = open(filesarray[file]['filepath'], 'rb')

    outbuffer = StringIO()
    outbuffer.write(infile.read())
    infile.close()

    # Fetch the archived file's entry infos (name, absolute offset once in the archive, size)
    indexentry = {}
    nameseedrw = nameseed + file + 1
    indexentry['filename'] = deobf_name(filesarray[file]['filename'], nameseedrw)
    indexentry['fileoffset'] = arcfile.tell()
    outbuffer.seek(0, os.SEEK_END)
    filesize = indexentry["filesize"] = outbuffer.tell()

    print 'Packing %s (%d bytes)' % (filesarray[file]['filename'], indexentry["filesize"])

    # Encrypt the fileoffset and filesize fields
    ciphertext = struct.pack('>I', indexentry['fileoffset'])
    ciphertext += struct.pack('>I', indexentry['filesize'])
    msg = cipher.encrypt(ciphertext)
    indexentry['fileoffset'] = (struct.unpack_from('>I', msg, 0)[0] - file - 1) & 0xFFFFFFFF
    indexentry['filesize'] = struct.unpack_from('>I', msg, 4)[0]

    # Encrypt the file
    # First: rotate all the DWORDs
    outbuffer.seek(0, os.SEEK_SET)
    data = b''
    roundlen = (filesize/8) * 8
    dwordslist = []
    for i in xrange(roundlen/4):
        dword = outbuffer.read(4)
        dwordslist.append(dword[::-1])
    data = ''.join(dwordslist)
    # Second: effectively encrypt the file
    data = cipher.encrypt(data)
    # Third: rotate all the DWORDs AGAIN,
    # then write the encrypted data to the output buffer
    intermediate = StringIO()
    intermediate.write(data)
    intermediate.seek(0)
    data = b''
    dwordslist = []
    for i in xrange(roundlen/4):
        dword = intermediate.read(4)
        dwordslist.append(dword[::-1])
    data = ''.join(dwordslist)
    intermediate.close()
    outbuffer.seek(0, os.SEEK_SET)
    outbuffer.write(data)
    outbuffer.seek(0, os.SEEK_END)

    if file < len(filesarray) - 1:
        write_entry(outbuffer, indexentry)
    outbuffer.seek(0, os.SEEK_SET)
    write_entry(indexbuffer, indexentry)
    arcfile.write(outbuffer.getvalue())

arcfile.seek(80, os.SEEK_SET)
arcfile.write(indexbuffer.getvalue())

arcfile.close()
