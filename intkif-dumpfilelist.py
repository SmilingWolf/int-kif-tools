# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

#!/usr/bin/env python

import sys, os, binascii, struct
from array import array
from cStringIO import StringIO
from Crypto.Cipher import Blowfish

import mt
from insani import *

import time
start = time.time()

if len(sys.argv) <> 2:
    print 'Please give an archive filename on the\ncommand line.'
    sys.exit(0)

def read_entry(infile):
    result = {}
    fileentry = infile.read(72)
    filename = fileentry
    fileentry = array('B', fileentry)

    filename = filename.split(b'\0',1)[0]
    result["filename"] = filename
    result["fileoffset"] = fileentry[64+3] << 24 | fileentry[64+2] << 16 | fileentry[64+1] << 8 | fileentry[64+0]
    result["filesize"] = fileentry[68+3] << 24 | fileentry[68+2] << 16 | fileentry[68+1] << 8 | fileentry[68+0]
    return result

# Pass to it an obfuscated name -> get the deobfuscated name
# Pass to it a deobfuscated name -> get the obfuscated name
def deobf_name(name, seed):
    newname = list(name)
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    revalpha = alpha[::-1]
    randkey = mt.MT19937(seed).extract_number()
    randkey = ((randkey >> 24) + (randkey >> 16) + (randkey >> 8) + randkey) & 0xFF
    for nameletters in xrange(len(name)):
        for alphaletters in xrange(len(revalpha)):
            index = (randkey + alphaletters) % len(revalpha)
            if name[nameletters] == revalpha[index]:
                newname[nameletters] = alpha[alphaletters-nameletters]
                break
    name = ''.join(newname)
    return name

def calc_filenameseed(v_key):
    magic = 0x4C11DB7
    seed = 0xFFFFFFFF
    for char in xrange(len(v_key)):
        letter = ord(v_key[char])
        letter = letter << 24
        seed ^= letter
        for i in xrange(8):
            if seed > 0x7FFFFFFF:
                seed += seed
                seed &= 0xFFFFFFFF
                seed ^= magic
            else:
                seed += seed
                seed &= 0xFFFFFFFF
        seed = ~seed
        seed &= 0xFFFFFFFF
    return seed

arcfile = open(sys.argv[0x0001], 'rb')
assert_string(arcfile, '\x4B\x49\x46\x00', ERROR_ABORT)
filenum = read_unsigned(arcfile)
nameseed = calc_filenameseed('FW-UPL9EX5T')

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    if indexarray[elem]['filename'] == '__key__.dat':
        continue

    nameseedrw = nameseed + elem
    indexarray[elem]['filename'] = deobf_name(indexarray[elem]['filename'], nameseedrw)

    print indexarray[elem]['filename'].decode('shift-jis').encode('utf-8')

arcfile.close()
