# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

#!/usr/bin/env python

import sys, os, binascii, struct
from array import array
from cStringIO import StringIO
from Crypto.Cipher import Blowfish

def decr_VCODE2(KEY_CODE, V_CODE2):
    KeyBytes = ''
    for char in KEY_CODE:
        KeyBytes += chr(char ^ 0xCD)

    VCode2Bytes = ''
    for index in xrange(0, 16, 4):
        VCode2Bytes += struct.pack('4B', *V_CODE2[index:index+4][::-1])

    #print 'Inizializing Blowfish. Key: %s (%s)' % (binascii.hexlify(KeyBytes).upper(), KeyBytes)
    cipher = Blowfish.new(KeyBytes, Blowfish.MODE_ECB)
    decrypted = cipher.decrypt(VCode2Bytes)

    V_CODE2d = ''
    for index in xrange(0, 16, 4):
        V_CODE2d += decrypted[index:index+4][::-1]
    return V_CODE2d

if __name__ == "__main__":
    # These values can be easily found in the .rsrc section of the game executable.

    # This appears to be the same for at least both editions of NekoPara Vol. 3
    KEY_CODE = [0xBA, 0xA4, 0xA3, 0xA9, 0xA0, 0xA4, 0xA1, 0xA1]

    # NekoPara Vol. 3 DLSite/DenpaSoft (cs2.exe)
    V_CODE2 = [0xEB, 0x6D, 0x8D, 0x78, 0x99, 0x63, 0x95, 0xB7, 0xEA, 0xEC, 0x0B, 0xF5, 0x71, 0xA4, 0xFC, 0xCD]
    res = decr_VCODE2(KEY_CODE, V_CODE2).rstrip('\00')
    print 'Expected: %s; result: %s; self-check: %s' % ('AMN-8ETQF49S', res, 'passed' if res == 'AMN-8ETQF49S' else 'not passed')

    # NekoPara Vol. 3 Steam (NEKOPARAvol3.exe)
    V_CODE2 = [0x63, 0xB4, 0x22, 0xDA, 0xBD, 0xA2, 0xF1, 0x7A, 0xA3, 0xE9, 0x0E, 0x48, 0xA7, 0x72, 0xAB, 0xDD]
    res = decr_VCODE2(KEY_CODE, V_CODE2).rstrip('\00')
    print 'Expected: %s; result: %s; self-check: %s' % ('AMN-018INN67', res, 'passed' if res == 'AMN-018INN67' else 'not passed')
