#!/usr/bin/env python

import sys, os, binascii, struct, zlib
from array import array
from cStringIO import StringIO

from insani import *

import time
start = time.time()

if len(sys.argv) <> 2:
    print 'Please give an archive filename on the\ncommand line.'
    sys.exit(0)

arcfile = open(sys.argv[1], 'rb')
assert_string(arcfile, '\x43\x61\x74\x53\x63\x65\x6E\x65', ERROR_ABORT)
compsize = read_unsigned(arcfile)
origsize = read_unsigned(arcfile)

print '%d -> %d' % (compsize, origsize)

data = zlib.decompress(arcfile.read(compsize))
outname = sys.argv[1]+'.unp.cst'
outfile = open(outname, 'wb')
outfile.write(data)
outfile.close

arcfile.close()
