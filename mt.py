# https://en.wikipedia.org/wiki/Mersenne_Twister#Python_implementation
# __init__ modified to reflect the Integer version (1999/10/28) from
# http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/VERSIONS/C-LANG/991029/mt19937int.c

def _int32(x):
    # Get the 32 least significant bits.
    return int(0xFFFFFFFF & x)

class MT19937:
    def __init__(self, seed):
        # Initialize the index to 0
        self.index = 624
        self.mt = [0] * 624
        self.mt[0] = seed  # Initialize the initial state to the seed
        for i in range(0, 624):
            self.mt[i] = seed & 0xFFFF0000
            seed = 69069 * seed + 1
            self.mt[i] |= (seed & 0xFFFF0000) >> 16
            seed = 69069 * seed + 1

    def extract_number(self):
        if self.index >= 624:
            self.twist()

        y = self.mt[self.index]

        # Right shift by 11 bits
        y = y ^ y >> 11
        # Shift y left by 7 and take the bitwise and of 0x9D2C5680
        y = y ^ y << 7 & 0x9D2C5680
        # Shift y left by 15 and take the bitwise and of y and 0xEFC60000
        y = y ^ y << 15 & 0xEFC60000
        # Right shift by 18 bits
        y = y ^ y >> 18

        self.index = self.index + 1

        return _int32(y)

    def twist(self):
        for i in range(624):
            # Get the most significant bit and add it to the less significant
            # bits of the next number
            y = _int32((self.mt[i] & 0x80000000) +
                       (self.mt[(i + 1) % 624] & 0x7fffffff))
            self.mt[i] = self.mt[(i + 397) % 624] ^ y >> 1

            if y % 2 != 0:
                self.mt[i] = self.mt[i] ^ 0x9908B0DF
        self.index = 0
