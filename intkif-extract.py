# Requires pycrypto. Install with:
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win-amd64-py2.7.exe
# if you're on win64 win python2.7 for 64bit or
# easy_install http://www.voidspace.org.uk/python/pycrypto-2.6.1/pycrypto-2.6.1.win32-py2.7.exe
# if you're using the 32bit environment

#!/usr/bin/env python

import sys, os, binascii, struct
from array import array
from cStringIO import StringIO
from Crypto.Cipher import Blowfish

import mt
from insani import *

import time
start = time.time()

if len(sys.argv) not in (3, 4):
    print 'Please give an archive filename and a desired output directory on the\ncommand line.'
    print 'Optional parameters: the password to deobfuscate the filenames'
    print '%s input_file output_dir [Filename_password]' % sys.argv[0]
    sys.exit(0)

def read_entry(infile):
    result = {}
    fileentry = infile.read(72)
    fileentry = array('B', fileentry)

    filename = array('B')
    for i in xrange(64):
        if fileentry[i] != 0x00:
            filename.append(fileentry[i])
    result["filename"] = filename.tostring()
    result["fileoffset"] = fileentry[64+3] << 24 | fileentry[64+2] << 16 | fileentry[64+1] << 8 | fileentry[64+0]
    result["filesize"] = fileentry[68+3] << 24 | fileentry[68+2] << 16 | fileentry[68+1] << 8 | fileentry[68+0]
    return result

# Pass to it an obfuscated name -> get the deobfuscated name
# Pass to it a deobfuscated name -> get the obfuscated name
def deobf_name(name, seed):
    newname = list(name)
    alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    revalpha = alpha[::-1]
    randkey = mt.MT19937(seed).extract_number()
    randkey = ((randkey >> 24) + (randkey >> 16) + (randkey >> 8) + randkey) & 0xFF
    for nameletters in xrange(len(name)):
        for alphaletters in xrange(len(revalpha)):
            index = (randkey + alphaletters) % len(revalpha)
            if name[nameletters] == revalpha[index]:
                newname[nameletters] = alpha[alphaletters-nameletters]
                break
    name = ''.join(newname)
    return name

def calc_filenameseed(v_key):
    magic = 0x4C11DB7
    seed = 0xFFFFFFFF
    for char in xrange(len(v_key)):
        letter = ord(v_key[char])
        letter = letter << 24
        seed ^= letter
        for i in xrange(8):
            if seed > 0x7FFFFFFF:
                seed += seed
                seed &= 0xFFFFFFFF
                seed ^= magic
            else:
                seed += seed
                seed &= 0xFFFFFFFF
        seed = ~seed
        seed &= 0xFFFFFFFF
    return seed

arcfile = open(sys.argv[0x0001], 'rb')
dirname = sys.argv[0x0002]
assert_string(arcfile, '\x4B\x49\x46\x00', ERROR_ABORT)
filenum = read_unsigned(arcfile)
print ('Extracting %d files' % filenum)
if len(sys.argv) < 4:
    # From DenpaSoft's Fruit of Grisaia Unrated Version
    nameseed = calc_filenameseed('FW-UPL9EX5T')
else:
    nameseed = calc_filenameseed(sys.argv[3])

indexarray = []
for i in xrange(filenum):
    fileentry = read_entry(arcfile)
    indexarray.append(fileentry)

for elem in xrange(len(indexarray)):
    if indexarray[elem]['filename'] == '__key__.dat':
        mtseed = indexarray[elem]['filesize']
        print 'Seeding the Mersenne Twister PRNG. Seed: %08X' % mtseed
        mersenne = mt.MT19937(mtseed)
        blowfishkey = struct.pack("<I", mersenne.extract_number())
        print 'Inizializing Blowfish. Key: %s' % binascii.hexlify(blowfishkey).upper()
        cipher = Blowfish.new(blowfishkey, Blowfish.MODE_ECB)
    else:
        ciphertext = struct.pack('>I', (indexarray[elem]['fileoffset'] + elem) & 0xFFFFFFFF) + struct.pack('>I', indexarray[elem]['filesize'])
        msg = cipher.decrypt(ciphertext)
        indexarray[elem]['fileoffset'] = struct.unpack_from('>I', msg, 0)[0]
        indexarray[elem]['filesize'] = struct.unpack_from('>I', msg, 4)[0]

print 'Setup time:', time.time()-start, 'seconds.'

for elem in xrange(len(indexarray)):
    if indexarray[elem]['filename'] == '__key__.dat':
        continue

    nameseedrw = nameseed + elem
    indexarray[elem]['filename'] = deobf_name(indexarray[elem]['filename'], nameseedrw)

    print 'Extracting %s (%d bytes)' % (indexarray[elem]['filename'], indexarray[elem]['filesize'])
    pathcomponents = indexarray[elem]['filename'].split('/')
    filepath = dirname
    for direct in pathcomponents:
        if not os.path.isdir(filepath):  # Create directory if it's not there
            os.mkdir(filepath)           # Won't do this for the final filename
        filepath = os.path.join(filepath, direct)

    arcfile.seek(indexarray[elem]['fileoffset'], 0)
    roundlen = (indexarray[elem]['filesize']/8) * 8
    firstrottime = time.time()
    dwordslistformat1 = '>%dI' % (roundlen/4)
    dwordslistformat2 = '<%dI' % (roundlen/4)
    data = struct.pack(dwordslistformat2, *struct.unpack(dwordslistformat1, arcfile.read(roundlen)))
    # print 'File dword #1 rotation took:', time.time()-firstrottime, 'seconds.'
    decryptstarttime = time.time()
    data = cipher.decrypt(data)
    # print 'File decryption took:', time.time()-decryptstarttime, 'seconds.'

    secondrottime = time.time()
    dwordslistformat1 = '>%dI' % (roundlen/4)
    dwordslistformat2 = '<%dI' % (roundlen/4)
    data = struct.pack(dwordslistformat2, *struct.unpack(dwordslistformat1, data))
    # print 'File dword #2 rotation took:', time.time()-secondrottime, 'seconds.'
    arcfile.seek(indexarray[elem]['fileoffset'] + roundlen, 0)
    data += arcfile.read(indexarray[elem]['filesize'] - roundlen)

    outfile = open(filepath, "wb")
    outfile.write(data)
    outfile.close()

arcfile.close()
print 'Total time:', time.time()-start, 'seconds.'
